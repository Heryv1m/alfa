package ua.teens.alfa.lesson6;

import java.util.Scanner;

import ua.teens.alfa.lesson7.Equation;

public class Main {

    public static void main(String[] args) {
        System.out.println("<-- Start program -->");

        while (true) {
            System.out.println("Select the equation to resolve: ");
            System.out.println("1) ax + bx = c");
            System.out.println("2) ax^2 + bx + c = 0");
            System.out.print("Enter number of selected equation: ");

            Scanner scannerMain = new Scanner(System.in);
            char[] varNames = new char[] { 'a', 'b', 'c' };
            double[] varValues = new double[3];

            Double numRes;
            boolean check;

            String selectedCase = scannerMain.nextLine();
            if ("1".equals(selectedCase)) {
                Equation equation = new Equation(varNames);
                numRes = equation.resolve(scannerMain);

                if (numRes != null) {
                    System.out.println("Answer is: " + numRes);
                    System.out.println("Next round");
                } else {
                    System.out.println("Enter variable are wrong. Try input something else");
                }

            } else if ("2".equals(selectedCase)) {
                System.out.println("You selected second equation!");
                System.out.println("You must enter constants values");

                // сrib
                // a = varValues[0]
                // b = varValues[1]
                // c = varValues[2]
                varValues = readVariables(scannerMain, varNames, varValues);

                System.out.println("Your equation looks like >> "
                        + varValues[0]
                        + "x^2 + "
                        + varValues[1]
                        + "x + "
                        + varValues[2]
                        + " = 0");

                System.out.println("<-- Analytical resolving --> \n");
                System.out.println("Discriminant finding >> D = "
                        + varValues[1]
                        + "^2 - 4 * "
                        + varValues[0]
                        + " * "
                        + varValues[2]);

                // Math.paw() or a * a;
                double discr = Math.pow(varValues[1], 2) - 4 * varValues[0] * varValues[2];

                if (discr > 0) {
                    System.out.println("D = " + discr + " > 0. So we have two root of equation.");
                    System.out.println("x1 = (-"
                            + varValues[1]
                            + " + sqrt("
                            + discr
                            + ")) / (2 * "
                            + varValues[0]
                            + ")");

                    System.out.println("x1 = (-"
                            + varValues[1]
                            + " - sqrt("
                            + discr
                            + ")) / (2 * "
                            + varValues[0]
                            + ")");

                    double x1 = (-varValues[1] + Math.sqrt(discr)) / (2 * varValues[0]);
                    double x2 = (-varValues[1] - Math.sqrt(discr)) / (2 * varValues[0]);

                    System.out.println("x1 = " + x1);
                    System.out.println("x2 = " + x2);

                    boolean check1 = 0 == (varValues[0] * Math.pow(x1, 2d) + varValues[1] * x1 + varValues[2]);
                    boolean check2 = 0 == (varValues[0] * Math.pow(x2, 2d) + varValues[1] * x2 + varValues[2]);

                    System.out.println("Check x1 is: " + check1);
                    System.out.println("Check x2 is: " + check2);

                    System.out.print("Answer is: ");
                    if (check1) {
                        System.out.println("x1 = " + x1);
                    }

                    if (check2) {
                        System.out.println("x2 = " + x2);
                    }


                } else if (discr < 0) {
                    System.out.println("D = " + discr + " < 0. So we haven't any root of equation.");

                } else {
                    System.out.println("D = " + discr + ". So we have one root of equation.");
                    System.out.println("x1 = (-"
                            + varValues[1]
                            + ") / (2 * "
                            + varValues[0]
                            + ")");

                    double x = (-varValues[1])/ (2 * varValues[0]);
                    System.out.println("x = " + x);

                    check = 0 == (varValues[0] * Math.pow(x, 2d) + varValues[1] * x + varValues[2]);

                    System.out.println("Check is: " + check);

                    if (check) {
                        System.out.println("Answer is: " + x);
                    } else {
                        System.out.println("Something wrong. Tru another variable!");
                    }
                }

                System.out.println("Next round!");
                // ax^2 + bx + c = 0
            } else if ("stop".equals(selectedCase)) {
                System.out.println("<-- Program is closing -->");
                break;
            } else {
                System.out.println("<-- Program is closing -->");
                break;
            }

        }

        System.out.println("<-- Stop program -->");
    }


    private static double[] readVariables(Scanner scanner, char[] varNames, double[] varValues) {
        for (int i = 0; i < varNames.length; i++) {
            System.out.print(varNames[i] + " = ");
            varValues[i] = Integer.parseInt(scanner.nextLine());
        }

        return varValues;
    }
}
