package ua.teens.alfa.lesson12_13;


import java.math.BigDecimal;

import static ua.teens.alfa.lesson12_13.ArenaUtils.*;

public class Start {

    public static void main(String[] args) {
        String[][] heroes = new String[7][7];

        heroes[0] = new String[]{
                "no",
                "team",
                "name",
                "attackPower",
                "maxHealth",
                "currentHealth",
                "armor",
                "live"
        };

        heroes[1] = createHero(1, "dc", "Batman");
        heroes[2] = createHero(2, "marvel", "Iron Man");
        heroes[3] = createHero(3, "marvel", "Captain America");
        heroes[4] = createHero(4, "dc", "Wonder Woman");
        heroes[5] = createHero(5, "dc", "Super Man");
        heroes[6] = createHero(6, "marvel", "Thor");

        showHeroesTable(heroes);

        String[][] winner = fight(heroes);

        showWinner(winner);
    }

    private static String[] createHero(int no, String team, String name) {
        BigDecimal attackPower = getCharacteristic(100);
        String maxHealth = "100";
        BigDecimal armor = getCharacteristic(10);

        return new String[]{
                String.valueOf(no),
                team,
                name,
                attackPower.toString(),
                maxHealth,
                maxHealth,
                armor.toString(),
                "+"
        };
    }

    private static String[][] fight(String[][] heroesTable) {
        boolean fight = true;
        String[][] winner = null;

        String k_armor = "1";
        String k_att = "1";
        int round = 0;

        while (fight) {
            String[][] heroesForFight = getHeroesForFight(heroesTable);

            if (heroesForFight.length > 0) {
                if (heroesForFight[0] != null && heroesForFight[1] != null) {
                    BigDecimal[] heroDamages = new BigDecimal[2];

                    for (int i = 0; i < 2; i++) {
                        heroDamages[i] = ArenaUtils.multiply(heroesForFight[i][3], k_att)
                                .subtract(ArenaUtils.multiply(heroesForFight[i][6], k_armor));
                        if (heroDamages[i].doubleValue() < 0) {
                            heroDamages[i] = BigDecimal.ZERO;
                        }
                    }

                    heroesForFight[0][5] = ArenaUtils.subtract(heroesForFight[0][5], heroDamages[1].toString()).toString();
                    heroesForFight[1][5] = ArenaUtils.subtract(heroesForFight[1][5], heroDamages[0].toString()).toString();

                    heroesTable = updateHeroesTable(heroesTable, heroesForFight);
                    round++;
                    System.out.println("Current round is - " + round);
                    showHeroesTable(heroesTable);
                } else {
                    winner = new String[][]{heroesTable[0], heroesForFight[0]};
                    fight = false;
                }
            } else {
                winner = new String[][]{};
                fight = false;
            }
        }

        return winner;
    }

    private static String[][] getHeroesForFight(String[][] heroesTable) {
        String[][] heroes = new String[2][];

        for (int heroCounter = 1; heroCounter < heroesTable.length; heroCounter++) {
            String[] hero = heroesTable[heroCounter];
            if ("+".equals(hero[7])) {
                if (heroes[0] == null) {
                    heroes[0] = hero;
                } else if (!(heroes[0][1].equals(hero[1]))){
                    heroes[1] = hero;
                }
            }
        }

        return (heroes[0] == null) ? new String[][]{} : heroes;
    }

    private static String[][] updateHeroesTable(String[][] heroesTable, String[][] heroFromFight) {
        for (int i = 0; i < heroesTable.length; i++) {
            for (int j = 0; j < heroFromFight.length; j++) {
                if (heroesTable[i][0].equals(heroFromFight[j][0])) {
                    if (Double.valueOf(heroFromFight[j][5]) > 0) {
                        heroesTable[i][5] = heroFromFight[j][5];
                    } else {
                        heroesTable[i][5] = "0";
                        heroesTable[i][7] = "-";
                    }
                }
            }
        }
        return heroesTable;
    }
}
