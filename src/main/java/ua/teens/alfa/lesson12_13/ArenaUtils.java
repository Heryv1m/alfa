package ua.teens.alfa.lesson12_13;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class ArenaUtils {

    private static final MathContext MC = new MathContext(2, RoundingMode.CEILING);
    private static final String LEFT_ALIGN_FORMAT =
            "| %-4s | %-8s | %-16s | %-9s | %-8s | %-9s | %-7s | %-4s |%n";

    public static BigDecimal multiply(String first, String second) {
        return BigDecimal.valueOf(Double.valueOf(first))
                .multiply(BigDecimal.valueOf(Double.valueOf(second)), MC);
    }

    public static BigDecimal subtract(String first, String second) {
        return BigDecimal.valueOf(Double.valueOf(first))
                .subtract(BigDecimal.valueOf(Double.valueOf(second)), MC);
    }

    public static BigDecimal getCharacteristic(int scale) {
        BigDecimal characteristic = null;
        boolean stop = false;
        while (!stop) {
            if (characteristic == null || characteristic.compareTo(BigDecimal.ZERO) < 1) {
                characteristic = multiply(
                        BigDecimal.valueOf(Math.random()).toString(),
                        String.valueOf(scale));
            } else {
                stop = true;
            }
        }
        return characteristic;
    }

    public static void showHeroesTable(String[][] heroesTable) {
        System.out.println("+------+----------+------------------+-----------+----------+-----------+---------+------+");
        System.out.println("|  NO  |   TEAM   |       NAME       |  ATT POW  |  MAX HP  |  CURR HP  |  ARMOR  | LIVE |");
        System.out.println("+------+----------+------------------+-----------+----------+-----------+---------+------+");
        for (int i = 1; i < heroesTable.length; i++) {
            System.out.format(LEFT_ALIGN_FORMAT,
                    heroesTable[i][0],
                    heroesTable[i][1],
                    heroesTable[i][2],
                    heroesTable[i][3],
                    heroesTable[i][4],
                    heroesTable[i][5],
                    heroesTable[i][6],
                    heroesTable[i][7]);
        }
        System.out.println("+------+----------+------------------+-----------+----------+-----------+---------+------+");
    }

    public static void showWinner(String[][] heroTable) {
        if (heroTable.length > 0) {
            showHeroesTable(heroTable);
        } else {
            System.out.println("No WINNER");
        }
    }
}
