package ua.teens.alfa.lesson11;

import java.util.ArrayList;

public class Car {

    private ArrayList<Wheel> wheel;
    private Body body;
    private Motor motor;
    private String color;

    public Car() {
    }

    public Car(Body body, Motor motor) {
        this.wheel = new ArrayList<>();
        this.body = body;
        this.motor = motor;
        this.color = body.getColor();
    }

    public ArrayList<Wheel> getWheel() {
        return wheel;
    }

    public void setWheel(ArrayList<Wheel> wheel) {
        this.wheel = wheel;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void addWheel(Wheel wheel) {
        this.wheel.add(wheel);
    }

    @Override
    public String toString() {
        return "Car{" +
                "wheel=" + wheel +
                ", body=" + body +
                ", motor=" + motor +
                ", color='" + color + '\'' +
                '}';
    }
}
