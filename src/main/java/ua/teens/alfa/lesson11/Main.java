package ua.teens.alfa.lesson11;

import java.math.BigDecimal;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Wheel wheel1 = new Wheel();
        Wheel wheel2 = new Wheel();
        Wheel wheel3 = new Wheel();
        Wheel wheel4 = new Wheel();
        Wheel wheel5 = new Wheel();

        ArrayList<Wheel> wheels = new ArrayList<>();
        wheels.add(wheel1);
        // add non exists wheel
        wheels.add(wheel2);
        // wheels[1] = wheel2
//        wheels.add(6, wheel5);
        // wheels[0]
        Wheel wheel = wheels.get(0);

        boolean contains = wheels.contains(wheel1);
        // change exists wheel
        wheels.set(0, wheel4);
        wheels.remove(wheel1);
        int index = wheels.indexOf(wheel4);
//        wheels.addAll(3, new ArrayList<>());
        wheels.clear();


        Wheel wheel_a = new Wheel(
                "KievTrade",
                "KievTrade",
                "Titan",
                new BigDecimal(2.5),
                16);
        Wheel wheel_b = new Wheel(
                "KievTrade",
                "KievTrade",
                "Titan",
                new BigDecimal(2.5),
                16);
        Wheel wheel_c = new Wheel(
                "KievTrade",
                "KievTrade",
                "Titan",
                new BigDecimal(2.5),
                16);
        Wheel wheel_d = new Wheel(
                "KievTrade",
                "KievTrade",
                "Titan",
                new BigDecimal(2.5),
                16);

        Body body = new Body(
                "Carbon",
                "Greeb",
                4);
        Motor motor = new Motor("T-34", 10, 6, 1500);
        Car car = new Car(body, motor);
        car.addWheel(wheel_a);
        car.addWheel(wheel_b);
        car.addWheel(wheel_c);
        car.addWheel(wheel_d);

        System.out.println(car.toString());
        for (Wheel w : car.getWheel()) {
            System.out.println(w.getWheelPressure());
        }
    }
}
