package ua.teens.alfa.lesson11;

public class Motor {

    private String motorSerial;
    private int cylinderCount;
    private int motorVolume;
    private int power;

    public Motor() {
    }

    public Motor(String motorSerial, int cylindrCount, int volume, int power) {
        this.motorSerial = motorSerial;
        this.cylinderCount = cylindrCount;
        this.motorVolume = volume;
        this.power = power;
    }

    public String getMotorSerial() {
        return motorSerial;
    }

    public void setMotorSerial(String motorSerial) {
        this.motorSerial = motorSerial;
    }

    public int getCylindrCount() {
        return cylinderCount;
    }

    public void setCylindrCount(int cylindrCount) {
        this.cylinderCount = cylindrCount;
    }

    public int getVolume() {
        return motorVolume;
    }

    public void setVolume(int volume) {
        this.motorVolume = volume;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Motor{" +
                "motorSerial='" + motorSerial + '\'' +
                ", cylindrCount=" + cylinderCount +
                ", volume=" + motorVolume +
                ", power=" + power +
                '}';
    }
}
