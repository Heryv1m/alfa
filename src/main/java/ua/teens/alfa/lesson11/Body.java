package ua.teens.alfa.lesson11;

public class Body {

    private String material;
    private String color;
    private int doorCount;

    public Body() {
    }

    public Body(String material, String color, int doorCount) {
        this.material = material;
        this.color = color;
        this.doorCount = doorCount;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    @Override
    public String toString() {
        return "Body{" +
                "material='" + material + '\'' +
                ", color='" + color + '\'' +
                ", doorCount=" + doorCount +
                '}';
    }
}
