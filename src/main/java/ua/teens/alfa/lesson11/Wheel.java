package ua.teens.alfa.lesson11;

import java.math.BigDecimal;

public class Wheel {

    private String tireProducer;
    private String discProducer;
    private String discMaterial;
    private BigDecimal wheelPressure;
    private int wheelDiameter;

    public Wheel() {
    }

    public Wheel(String tireProducer, String discProducer, String discMaterial, BigDecimal wheelPressure, int wheelDiameter) {
        this.tireProducer = tireProducer;
        this.discProducer = discProducer;
        this.discMaterial = discMaterial;
        this.wheelPressure = wheelPressure;
        this.wheelDiameter = wheelDiameter;
    }

    public String getTireProducer() {
        return tireProducer;
    }

    public void setTireProducer(String tireProducer) {
        this.tireProducer = tireProducer;
    }

    public String getDiscProducer() {
        return discProducer;
    }

    public void setDiscProducer(String discProducer) {
        this.discProducer = discProducer;
    }

    public String getDiscMaterial() {
        return discMaterial;
    }

    public void setDiscMaterial(String discMaterial) {
        this.discMaterial = discMaterial;
    }

    public BigDecimal getWheelPressure() {
        return wheelPressure;
    }

    public void setWheelPressure(BigDecimal wheelPressure) {
        this.wheelPressure = wheelPressure;
    }

    public int getWheelDiameter() {
        return wheelDiameter;
    }

    public void setWheelDiameter(int wheelDiameter) {
        this.wheelDiameter = wheelDiameter;
    }

    @Override
    public String toString() {
        return "Wheel{" +
                "tireProducer='" + tireProducer + '\'' +
                ", discProducer='" + discProducer + '\'' +
                ", discMaterial='" + discMaterial + '\'' +
                ", wheelPressure=" + wheelPressure +
                ", wheelDiameter=" + wheelDiameter +
                '}';
    }
}
