package ua.teens.alfa.lesson4.condition;

import java.util.Scanner;

public class Main {

//    static int a  = 6;
//    static int b = 6;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int a = Integer.parseInt(scanner.nextLine());
        int b = Integer.parseInt(scanner.nextLine());
        int c = Integer.parseInt(scanner.nextLine());

        if (a < b) {
            System.out.println("a < b");
        } else if (a == b) {
            System.out.println("a = b");
        } else {
            System.out.println("a > b");
        }

//        System.out.println(c <= 2 ? c == 0 ? "c == 0" : "c <= 2" : "c > 2");
        System.out.println(c <= 2 ? "c <= 2" : "c > 2");


    }
}
