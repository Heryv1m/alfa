package ua.teens.alfa.lesson4.condition;

import java.util.Scanner;

public class Main1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        switch (scanner.nextLine()) {
            case "line": {
                System.out.println("This is line");
                break;
            }

            case "car": {
                System.out.println("This is car");
                break;
            }
            case "котэ":
            case "cat": {
                System.out.println("This is cat");
                break;
            }

            default: {
                System.out.println("Exit");
            }
        }
    }
}
