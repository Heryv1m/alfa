package ua.teens.alfa.lesson4.loops;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        // for ([counter initialization]; [condition]; [counter changing])
//        for (int i = 0;;i++) {
////            if ((i % 100) == 0) {
////                System.out.println("Your cash increase to: $" + i);
////            } else {
////                System.out.println("$" + i);
////            }
////        }
        int j = 7;

//        do {
//            System.out.println(j);
//            j--;
//        } while (j > 7);

//        while (j > 0) {
//            System.out.println(j);
//            j--;
//        }

//        Scanner scanner = new Scanner(System.in);
//
//        String str;
//
//        while (true) {
//            str = scanner.nextLine();
//            System.out.println("Your string is: " + str);
//
//            if ("exit".equals(str)) {
//                break;
//            }
//        }

        System.out.println(Math.pow(2, 2));

        System.out.println();
    }
}
