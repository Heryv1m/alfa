package ua.teens.alfa.lesson5;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[3];
        int[] numTwo = new int[] { 1, 2, 3};

        num[0] = 1;
        num[1] = 2;
        num[2] = 3;
//        num[3] = 0;

        for (int i = 0; i < num.length; i++) {
//            System.out.println(num[i]);
            num[i] = i + 3;

            System.out.println(num[i]);
        }
    }
}
