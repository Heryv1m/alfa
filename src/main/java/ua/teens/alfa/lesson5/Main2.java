package ua.teens.alfa.lesson5;

public class Main2 {

//    static int b = 1;

    public static void main(String[] args) {
        int[][] a = createMatrixThreeXThree();
        matrixOutput(a);
    }

    /**
     * <access modifier>
     * <static or not>
     * <returned data type>
     * <method name>
     * <arguments>
     * <code block>
     */
    public static void matrixOutput(int[][] a) {
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.println(a[i][j]);
            }
        }
    }

    public static int[][] createMatrixThreeXThree() {
        int[][] a = new int[3][];

        a[0] = new int[]{1, 2, 3};
        a[1] = new int[]{4, 5, 6};
        a[2] = new int[]{7, 8, 9};

        return a;
    }
}
