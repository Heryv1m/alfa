package ua.teens.alfa.lesson15.animal;

public class MeatAnimal implements Animal{
    @Override
    public void eat() {
        System.out.println("Eat Meat");
    }
}
