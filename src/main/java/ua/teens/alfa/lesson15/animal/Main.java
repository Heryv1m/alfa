package ua.teens.alfa.lesson15.animal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Animal> animals = new ArrayList<>();
        List<Animal> animals2 = new LinkedList<>();
        animals.add(new VeganAnimal());
        animals.add(new MeatAnimal());

        for (Animal animal : animals) {
            animal.eat();
        }
    }
}
