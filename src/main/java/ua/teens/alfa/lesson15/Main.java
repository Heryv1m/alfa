package ua.teens.alfa.lesson15;

public class Main {
    public static void main(String[] args) {
        Parent parent = new Parent(1, "Aloha");
        Child child = new Child(2);
        Child childNew = new Child(3, "Volar", 20);
        Child2 child2 = new Child2(4, "Kate", "Freeze");
        Parent newParent = new Child2(5, "Goha", "Fire");

        System.out.println(parent.getName());

        System.out.println(child.getAge());
        System.out.println(child.getName());

        System.out.println(childNew.getAge());

        System.out.println(child2.getSkill());

        System.out.println(newParent.getName());

        System.out.println(get(newParent));
        System.out.println(get(childNew));
        System.out.println(get(child2));
        System.out.println(get(child));

        System.out.println("<<<<<<----->>>>>>");
        Driver driver = new Child(6, "Harry", 65);
        Driver driver2 = new Child2(7, "Danny", "Fly");
        printDrive(driver);
        printDrive(driver2);
    }

    public static int get(Parent parent) {
        return parent.getId();
    }

    public static void printDrive(Driver driver) {
        if (driver instanceof Child2) {
            System.out.println(((Child2) driver).getSkill());
        } else {
            driver.drive();
        }
    }
}
