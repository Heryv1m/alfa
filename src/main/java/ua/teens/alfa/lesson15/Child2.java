package ua.teens.alfa.lesson15;

public class Child2 extends Parent implements Driver {

    private String skill;

    public Child2() {
    }

    public Child2(String skill) {
        this.skill = skill;
    }

    public Child2(int id, String name, String skill) {
        super(id, name);
        this.skill = skill;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    @Override
    public void drive() {
        System.out.println("Drive car: " + super.getId());
    }
}
