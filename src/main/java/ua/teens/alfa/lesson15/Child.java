package ua.teens.alfa.lesson15;

public class Child extends Parent implements Driver {

    private int age;

    public Child() {
    }

    public Child(int age) {
        this.age = age;
    }

    public Child(int id, String name, int age) {
        super(id, name);
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public void drive() {
        System.out.println("Drive airplane: " + super.getId());
    }
}
