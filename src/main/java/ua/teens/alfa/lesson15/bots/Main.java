package ua.teens.alfa.lesson15.bots;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        String message = "Hello world!";

        List<Bot> bots = new ArrayList<>();
        bots.add(new FacebookBot());
        bots.add(new TelegramBot());
        bots.add(new ViberBot());

        for (Bot bot : bots) {
            bot.receive(message);
            bot.send();
        }
    }
}
