package ua.teens.alfa.lesson15.bots;

public class TelegramBot implements Bot {

    private String message;

    @Override
    public void receive(String message) {
        this.message = message;
    }

    @Override
    public void send() {
        System.out.println("Message: "
                + message);
    }
}
