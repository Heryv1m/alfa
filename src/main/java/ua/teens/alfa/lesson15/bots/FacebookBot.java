package ua.teens.alfa.lesson15.bots;

public class FacebookBot implements Bot {

    private String code;
    private String receiver;
    private String message;

    @Override
    public void receive(String message) {
        this.receiver = "Facebook";
        this.message = message;
        this.code = "Authy";
    }

    @Override
    public void send() {
        System.out.println("Message: "
                + message
                + " is from: "
                + receiver
                + " with code: "
                + code);
    }
}
