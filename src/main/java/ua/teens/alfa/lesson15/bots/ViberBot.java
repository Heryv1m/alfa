package ua.teens.alfa.lesson15.bots;

public class ViberBot implements Bot {

    private String receiver;
    private String message;

    @Override
    public void receive(String message) {
        this.receiver = "Viber";
        this.message = message;
    }

    @Override
    public void send() {
        System.out.println("Message: "
                + message
                + " is from: "
                + receiver);
    }
}
