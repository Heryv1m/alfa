package ua.teens.alfa.lesson15.bots;

public interface Bot {

    void receive(String message);
    void send();
}
