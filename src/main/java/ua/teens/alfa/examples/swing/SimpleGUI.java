package ua.teens.alfa.examples.swing;

import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class SimpleGUI extends JFrame {

    private Container mainPlain;

    private JButton button = new JButton("Press");
    private JTextField input = new JTextField("", 5);
    private JLabel label = new JLabel("Input:");
    private JRadioButton radio1 = new JRadioButton("Select this");
    private JRadioButton radio2 = new JRadioButton("Select that");
    private JCheckBox check = new JCheckBox("Check", false);

    public SimpleGUI() {
        super("Simple Example");
        this.setBounds(100, 100, 250, 100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        mainPlain = getContentPane();
        mainPlain.setLayout(new GridLayout(3, 2, 2, 2));
        mainPlain.add(label);
        mainPlain.add(input);

        ButtonGroup group = new ButtonGroup();
        group.add(radio1);
        group.add(radio2);
        mainPlain.add(radio1);

        radio1.setSelected(true);
        mainPlain.add(radio2);
        mainPlain.add(check);
        //button.addActionListener(new ButtonEventListener(input, radio1, radio2, check));
        button.addActionListener(e -> {
            mainPlain.removeAll();
            mainPlain.add(new JPanel());
            mainPlain.validate();
        });
        mainPlain.add(button);
    }
}