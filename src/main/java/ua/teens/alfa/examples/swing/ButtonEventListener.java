package ua.teens.alfa.examples.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class ButtonEventListener implements ActionListener {

    private JTextField input;
    private JRadioButton radio1;
    private JRadioButton radio2;
    private JCheckBox check;

    public ButtonEventListener(JTextField input, JRadioButton radio1, JRadioButton radio2, JCheckBox check) {
        this.input = input;
        this.radio1 = radio1;
        this.radio2 = radio2;
        this.check = check;
    }

    public void actionPerformed(ActionEvent e) {
        String message = "";
        message += "Button was pressed\n";
        message += "Text is " + input.getText() + "\n";
        message += (radio1.isSelected() ? "Radio #1" : "Radio #2") + " is selected\n";
        message += "CheckBox is " + ((check.isSelected()) ? "checked" : "unchecked");
        JOptionPane.showMessageDialog(null,
                message,
                "Output",
                JOptionPane.PLAIN_MESSAGE);
    }
}
