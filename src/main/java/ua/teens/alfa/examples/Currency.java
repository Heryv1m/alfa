package ua.teens.alfa.examples;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Currency {

    public static void main(String[] args) {
        // first double value
        BigDecimal a = new BigDecimal(15.2);
        // second double value
        BigDecimal b = new BigDecimal(1123.1);

        // divide and round with 3 digit after comma.
        BigDecimal solution = a.divide(b, 3, RoundingMode.DOWN);

        System.out.println(solution);
    }
}
