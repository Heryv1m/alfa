package ua.teens.alfa.lesson7;

import java.util.Scanner;

public class Equation {

    double[] varValues;
    char[] varNames;
    double numRes;
    boolean check;



    public Equation(char[] varNames) {
        this.varNames = varNames;
    }

    public Double resolve(Scanner scanner) {
        System.out.println("You selected first equation!");
        System.out.println("You must enter constants values");

        varValues = readVariables(scanner, varNames, varValues);

        System.out.print("Your equation looks like >> "
                + varValues[0]
                + "x + "
                + varValues[1]
                + "x = "
                + varValues[2]);

        System.out.println("<-- Analytical resolving --> \n");
        System.out.println("x ("
                + varValues[0]
                + " + "
                + varValues[1]
                + ") = "
                + varValues[2]);

        System.out.println("x = "
                + varValues[2]
                + " / ("
                + varValues[0]
                + " + "
                + varValues[1]
                + ")");

        numRes = varValues[2] / (varValues[0] + varValues[1]);

        check = varValues[2] == (numRes * varValues[0] + numRes * varValues[1]);

        if (check) {
            return numRes;
        } else {
            return null;
        }

    }

    private double[] readVariables(Scanner scanner, char[] varNames, double[] varValues) {
        for (int i = 0; i < varNames.length; i++) {
            System.out.print(varNames[i] + " = ");
            varValues[i] = Integer.parseInt(scanner.nextLine());
        }

        return varValues;
    }
}
