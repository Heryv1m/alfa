package ua.teens.alfa.lesson7;

import java.math.BigDecimal;

public class Computer {

    private Motherboard motherboard;
    private Videocard videocard;
    private PMemory pMemory;
    private Memory memory;

    public Computer(Motherboard motherboard, Videocard videocard, PMemory pMemory, Memory memory) {
        this.motherboard = motherboard;
        this.videocard = videocard;
        this.pMemory = pMemory;
        this.memory = memory;
    }

    public BigDecimal calculateCost() {
        return motherboard.cost
                .add(videocard.cost)
                .add(pMemory.cost)
                .add(memory.cost);
    }

    public Memory getMemory() {
        return this.memory;
    }
}
