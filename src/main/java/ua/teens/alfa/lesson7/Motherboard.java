package ua.teens.alfa.lesson7;

import java.math.BigDecimal;

public class Motherboard {

    String chipset;
    boolean bust;
    String socket;
    int socketValue;
    BigDecimal cost;

    public Motherboard(String chipset, boolean bust, String socket, int socketValue, BigDecimal cost) {
        this.chipset = chipset;
        this.bust = bust;
        this.socket = socket;
        this.socketValue = socketValue;
        this.cost = cost;
    }
}
