package ua.teens.alfa.lesson7;

import java.math.BigDecimal;

public class Memory {

    String name;
    double value;
    int speed;
    String madeIn;
    BigDecimal cost;

    public Memory(String name, double value, int speed, String madeIn, BigDecimal cost) {
        this.name = name;
        this.value = value;
        this.speed = speed;
        this.madeIn = madeIn;
        this.cost = cost;
    }
}
