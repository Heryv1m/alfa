package ua.teens.alfa.lesson7;

import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) {
        PMemory pMemory = new PMemory("WD", 1000, 5400, "China", new BigDecimal(400.40d));
        Memory memory = new Memory("Samsung", 4, 3200, "Germany", new BigDecimal(100.15d));
        Motherboard motherboard = new Motherboard("Sx7", true, "Gento", 2, new BigDecimal(200.1));
        Videocard videocard = new Videocard("GeForce", 2, 2500, "France", new BigDecimal(1000d));
        Videocard videocard1 = new Videocard("Chenhu", 2, 10000, "USA", new BigDecimal(10));

        Computer computer = new Computer(motherboard, videocard, pMemory, memory);
        Computer computer1= new Computer(motherboard, videocard1, pMemory, memory);

        Computer[] computers = new Computer[] { computer, computer1 };
    }
}
