package ua.teens.alfa.lesson10;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

public class Transaction {

    String sourceAccount;
    String destAccount;
    BigDecimal amount;
    LocalDateTime date;
    boolean successful;

    public Transaction(String sourceAccount,
                       String destAccount,
                       BigDecimal amount,
                       LocalDateTime date) {
        this.sourceAccount = sourceAccount;
        this.destAccount = destAccount;
        this.amount = amount;
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "sourceAccount='" + sourceAccount + '\'' +
                ", destAccount='" + destAccount + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                ", successful=" + successful +
                '}';
    }
}
