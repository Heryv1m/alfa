package ua.teens.alfa.lesson10;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Bill {

    public static void main(String[] args) {
        BigDecimal amount = new BigDecimal(14.1);
        Transaction transaction = new Transaction(
                "Bogdana",
                "Ruslana",
//            amount,
                amount.setScale(2, BigDecimal.ROUND_CEILING),
                LocalDateTime.now());

        System.out.println(transaction.toString());
    }
}
