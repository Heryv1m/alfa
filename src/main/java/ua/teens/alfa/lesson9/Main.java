package ua.teens.alfa.lesson9;

public class Main {

    public static void main(String[] args) {
        Animal crocodile = new Animal("crocodile", false, "Croco");
        Animal crocodile1 = new Animal("crocodile", false, "Saber");
        Animal parrot = new Animal("parrot", true, "Ara");
        Animal parrot1 = new Animal("parrot", true, "Crack");
        Animal horse = new Animal("horse", true, "Yuli");
        Animal horse1 = new Animal("horse", true, "Ran");
        Animal lion = new Animal("lion", false, "Simba");

        Cage cageOne = AnimalSorter.sorter(crocodile, crocodile1);
        Cage cageTwo = new Cage(parrot, lion);
        System.out.println(cageOne.toString());
        System.out.println(cageTwo.toString());
        CageGroup cageGroup = new CageGroup(1);
        cageGroup.cages[0] = cageOne;
        cageGroup.cages[1] = cageTwo;
        System.out.println(cageGroup.toString());
    }
}
