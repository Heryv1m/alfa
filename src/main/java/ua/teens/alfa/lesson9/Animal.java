package ua.teens.alfa.lesson9;

public class Animal {

    String type;
    String name;
    boolean vegan;

    public Animal(String type, boolean vegan, String name) {
        this.name = name;
        this.type = type;
        this.vegan = vegan;

    }

    @Override
    public String toString() {
        return "Animal{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", vegan=" + vegan +
                '}';
    }
}
