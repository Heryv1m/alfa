package ua.teens.alfa.lesson9;

public class AnimalSorter {

    public static Cage sorter(Animal one, Animal two) {
        Cage cage;
        if ((one.vegan && !two.vegan) ||
                (!one.vegan && two.vegan)) {
//            throw new RuntimeException();
            System.out.println("Animals not sorted. Cause: one of them vegan");
            cage = null;
        } else {
            cage = new Cage(one, two);
        }

        return cage;
    }
}
