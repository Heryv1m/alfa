package ua.teens.alfa.lesson9;

import java.util.Arrays;

public class Cage {

    Animal[] animals = new Animal[2];
    boolean vegan;

    public Cage(Animal anOne, Animal anTwo) {
        animals[0] = anOne;
        animals[1] = anTwo;
        this.vegan = anOne.vegan;
    }

    @Override
    public String toString() {
        return "Cage{" +
                "animals=" + Arrays.toString(animals) +
                ", vegan=" + vegan +
                '}';
    }
}
