package ua.teens.alfa.lesson9;

import java.util.Arrays;

public class CageGroup {

    Cage[] cages = new Cage[25];
    int number;

    public CageGroup(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "CageGroup{" +
                "cages=" + Arrays.toString(cages) +
                ", number=" + number +
                '}';
    }
}