package ua.teens.alfa.lesson14.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        String a = "a";
        String b = "b";
        String c = "c";

        ArrayList<String> strList = new ArrayList<>();
        strList.add(a);
        strList.add(b);
        strList.add(c);
        HashSet<String> strSet = new HashSet<>();
        strSet.add(a);
        strSet.add(b);
        strSet.add("d");
        strSet.add(c);
        strSet.add("e");
        strSet.add("f");
        HashMap<String, Integer> map = new HashMap<>();
        map.put("Nikolay", 10);
        map.put("Nikita", 10);
        map.put("Derik", 10);
        map.put("Ruslana", 10);
        map.put("Danil", 10);

        for (Map.Entry<String, Integer> std: map.entrySet()) {
            System.out.println(std.getKey() + " : " + std.getValue());
        }

        System.out.println(map.get("Danil"));


        for (String s : strList) {
            System.out.println(s);
        }

        for (String s: strSet) {
            System.out.println(s);
        }
    }
}
