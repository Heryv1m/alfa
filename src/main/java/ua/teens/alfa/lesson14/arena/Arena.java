package ua.teens.alfa.lesson14.arena;

import java.math.BigDecimal;
import java.util.ArrayList;

import ua.teens.alfa.lesson14.arena.builder.ArenaBuilder;
import ua.teens.alfa.lesson14.arena.model.Hero;
import ua.teens.alfa.lesson14.arena.model.HeroTeam;
import ua.teens.alfa.lesson14.arena.model.Round;
import ua.teens.alfa.lesson14.arena.utils.ArenaUtils;

import static ua.teens.alfa.lesson14.arena.utils.ArenaUtils.*;

public class Arena {

    private static final ArrayList<HeroTeam> heroTeams = new ArrayList<>();

    public static void main(String[] args) {
        HeroTeam dc = ArenaBuilder.createTeam(1L, "DC");
        dc.addHero(1L, "Super Man");
        dc.addHero(2L, "Wonder Woman");
        dc.addHero(3L, "Batman");

        HeroTeam marvel = ArenaBuilder.createTeam(2L, "MARVEL");
        marvel.addHero(1L, "Iron Man");
        marvel.addHero(2L, "Captain America");
        marvel.addHero(3L, "Thor");

        heroTeams.add(dc);
        heroTeams.add(marvel);

        showHeroesTable(heroTeams);

        Hero winner = fight(heroTeams);

        for (HeroTeam heroTeam : heroTeams) {
            if (heroTeam.getHeroes().contains(winner)) {
                ArenaUtils.showWinner(winner, heroTeam.getTeamName());
            }
        }
    }

    public static Hero fight(ArrayList<HeroTeam> heroTeams) {
        boolean fight = true;
        Hero winner = null;
        int roundNum = 0;

        BigDecimal k_armor = BigDecimal.ONE;
        BigDecimal k_att = BigDecimal.ONE;

        while (fight) {
            ArrayList<Hero> heroesForFight = getHeroesForFight(heroTeams);
            if (heroesForFight.size() == 2) {
                    Round round = new Round(heroesForFight.get(0), heroesForFight.get(1), k_armor, k_att);
                    round.calculateDps();

                    ArrayList<HeroTeam> updatedHeroesTeams = updateHeroesTable(heroTeams, round);

                    roundNum++;
                    System.out.println("Current roundNum is - " + roundNum);
                    showHeroesTable(updatedHeroesTeams);
            } else if (heroesForFight.size() == 1) {
                winner = heroesForFight.get(0);
                fight = false;
            } else {
                winner = null;
                fight = false;
            }
        }

        return winner;
    }
}
