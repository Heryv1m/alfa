package ua.teens.alfa.lesson14.arena.model;

import java.util.ArrayList;
import java.util.Objects;

import ua.teens.alfa.lesson14.arena.builder.ArenaBuilder;

public class HeroTeam {

    private Long teamNo;
    private String teamName;
    private ArrayList<Hero> heroes;

    public HeroTeam(String teamName, Long teamNo) {
        this.teamName = teamName;
        this.teamNo = teamNo;
        this.heroes = new ArrayList<>();
    }

    public HeroTeam(String teamName, ArrayList<Hero> heroes, Long teamNo) {
        this.teamName = teamName;
        this.teamNo = teamNo;
        this.heroes = heroes;
    }

    public void addHero(Long no, String name) {
        Hero hero = ArenaBuilder.createHero(no, name);
        hero.setTeamNo(this.teamNo);
        heroes.add(hero);
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public ArrayList<Hero> getHeroes() {
        return heroes;
    }

    public void setHeroes(ArrayList<Hero> heroes) {
        this.heroes = heroes;
    }

    public Long getTeamNo() {
        return teamNo;
    }

    public void setTeamNo(Long teamNo) {
        this.teamNo = teamNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HeroTeam)) return false;
        HeroTeam heroTeam = (HeroTeam) o;
        return Objects.equals(getTeamNo(), heroTeam.getTeamNo()) &&
                Objects.equals(getTeamName(), heroTeam.getTeamName()) &&
                Objects.equals(getHeroes(), heroTeam.getHeroes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTeamNo(), getTeamName(), getHeroes());
    }

    @Override
    public String toString() {
        return "HeroTeam{" +
                "teamNo=" + teamNo +
                ", teamName='" + teamName + '\'' +
                ", heroes=" + heroes +
                '}';
    }
}
