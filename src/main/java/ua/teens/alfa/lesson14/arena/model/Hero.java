package ua.teens.alfa.lesson14.arena.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Hero {

    private Long no;
    private Long teamNo;
    private String heroName;
    private BigDecimal attackPower;
    private BigDecimal maxHealth;
    private BigDecimal currHealth;
    private BigDecimal armor;
    private boolean alive;
    // and so on

    public Hero() {
    }

    public Hero(Long no, Long teamNo, String heroName, BigDecimal attackPower,
                BigDecimal maxHealth, BigDecimal currHealth, BigDecimal armor, boolean alive) {
        this.no = no;
        this.teamNo = teamNo;
        this.heroName = heroName;
        this.attackPower = attackPower;
        this.maxHealth = maxHealth;
        this.currHealth = currHealth;
        this.armor = armor;
        this.alive = alive;
    }

    public Long getNo() {
        return no;
    }

    public void setNo(Long no) {
        this.no = no;
    }

    public Long getTeamNo() {
        return teamNo;
    }

    public void setTeamNo(Long teamNo) {
        this.teamNo = teamNo;
    }

    public String getHeroName() {
        return heroName;
    }

    public void setHeroName(String heroName) {
        this.heroName = heroName;
    }

    public BigDecimal getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(BigDecimal attackPower) {
        this.attackPower = attackPower;
    }

    public BigDecimal getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(BigDecimal maxHealth) {
        this.maxHealth = maxHealth;
    }

    public BigDecimal getCurrHealth() {
        return currHealth;
    }

    public void setCurrHealth(BigDecimal currHealth) {
        this.currHealth = currHealth;
    }

    public BigDecimal getArmor() {
        return armor;
    }

    public void setArmor(BigDecimal armor) {
        this.armor = armor;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hero)) return false;
        Hero hero = (Hero) o;
        return isAlive() == hero.isAlive() &&
                Objects.equals(getNo(), hero.getNo()) &&
                Objects.equals(getTeamNo(), hero.getTeamNo()) &&
                Objects.equals(getHeroName(), hero.getHeroName()) &&
                Objects.equals(getAttackPower(), hero.getAttackPower()) &&
                Objects.equals(getMaxHealth(), hero.getMaxHealth()) &&
                Objects.equals(getCurrHealth(), hero.getCurrHealth()) &&
                Objects.equals(getArmor(), hero.getArmor());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNo(), getTeamNo(), getHeroName(), getAttackPower(), getMaxHealth(), getCurrHealth(), getArmor(), isAlive());
    }

    @Override
    public String toString() {
        return "Hero{" +
                "no=" + no +
                ", teamNo=" + teamNo +
                ", heroName='" + heroName + '\'' +
                ", attackPower=" + attackPower +
                ", maxHealth=" + maxHealth +
                ", currHealth=" + currHealth +
                ", armor=" + armor +
                ", alive=" + alive +
                '}';
    }
}
