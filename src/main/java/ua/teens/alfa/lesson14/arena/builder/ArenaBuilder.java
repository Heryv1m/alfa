package ua.teens.alfa.lesson14.arena.builder;

import java.math.BigDecimal;
import java.util.ArrayList;

import ua.teens.alfa.lesson14.arena.model.Hero;
import ua.teens.alfa.lesson14.arena.model.HeroTeam;
import ua.teens.alfa.lesson14.arena.utils.ArenaUtils;

public class ArenaBuilder {

    public static Hero createHero(Long no, String heroName) {
        BigDecimal armor = ArenaUtils.getArmor();
        BigDecimal attPow = ArenaUtils.getAttackPower();
        return new Hero(no, null, heroName, attPow, BigDecimal.valueOf(100L), BigDecimal.valueOf(100L), armor, true);
    }

    public static HeroTeam createTeam(Long teamNo, String heroTeam, ArrayList<Hero> heroes) {
        return new HeroTeam(heroTeam, heroes, teamNo);
    }

    public static HeroTeam createTeam(Long teamNo, String heroTeam) {
        return new HeroTeam(heroTeam, teamNo);
    }
}
