package ua.teens.alfa.lesson14.arena.utils;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;

import ua.teens.alfa.lesson14.arena.model.Hero;
import ua.teens.alfa.lesson14.arena.model.HeroTeam;
import ua.teens.alfa.lesson14.arena.model.Round;

public class ArenaUtils {

    public static final MathContext MC = new MathContext(2, RoundingMode.CEILING);
    private static final String LEFT_ALIGN_FORMAT =
            "| %-4s | %-8s | %-16s | %-9s | %-8s | %-9s | %-7s | %-4s |%n";

    public static BigDecimal getArmor() {
        return BigDecimal.valueOf(Math.random()).multiply(BigDecimal.TEN, ArenaUtils.MC);
    }

    public static BigDecimal getAttackPower() {
        return BigDecimal.valueOf(Math.random()).multiply(BigDecimal.valueOf(100), ArenaUtils.MC);
    }

    public static void showHeroesTable(ArrayList<HeroTeam> heroTeams) {
        printHeader();
        for (int i = 0; i < heroTeams.size(); i++) {
            String teamName = heroTeams.get(i).getTeamName();
            for (int j = 0; j < heroTeams.get(i).getHeroes().size(); j++) {
                Hero hero = heroTeams.get(i).getHeroes().get(j);
                printHero(hero, teamName);
            }
            printFooter();
        }
    }

    public static void showWinner(Hero winner, String teamName) {
        if (winner != null) {
            printHeader();
            printHero(winner, teamName);
            printFooter();
        } else {
            System.out.println("No WINNER");
        }
    }

    public static void printHeader() {
        System.out.println("+------+----------+------------------+-----------+----------+-----------+---------+------+");
        System.out.println("|  NO  |   TEAM   |       NAME       |  ATT POW  |  MAX HP  |  CURR HP  |  ARMOR  | LIVE |");
        System.out.println("+------+----------+------------------+-----------+----------+-----------+---------+------+");
    }

    public static void printFooter() {
        System.out.println("+------+----------+------------------+-----------+----------+-----------+---------+------+");
    }

    public static void printHero(Hero hero, String teamName) {
        System.out.format(LEFT_ALIGN_FORMAT,
                hero.getNo(),
                teamName,
                hero.getHeroName(),
                hero.getAttackPower(),
                hero.getMaxHealth(),
                hero.getCurrHealth(),
                hero.getArmor(),
                hero.isAlive());
    }

    public static ArrayList<Hero> getHeroesForFight(ArrayList<HeroTeam> heroTeams) {
        ArrayList<Hero> heroesForFight = new ArrayList<>();
        for (HeroTeam team : heroTeams) {
            for (Hero hero : team.getHeroes()) {
                if (hero.isAlive()) {
                    heroesForFight.add(hero);
                    break;
                }
            }
        }

        return heroesForFight;
    }

    public static ArrayList<HeroTeam> updateHeroesTable(ArrayList<HeroTeam> heroTeams, Round round) {
        BigDecimal currHPForFirstHero = round.getFirstHero()
                .getCurrHealth()
                .subtract(round.getDpsForFirstHero());
        BigDecimal currHPForSecondHero = round.getSecondHero()
                .getCurrHealth()
                .subtract(round.getDpsForSecondHero());

        round.getFirstHero().setCurrHealth(currHPForFirstHero);
        round.getSecondHero().setCurrHealth(currHPForSecondHero);

        for (HeroTeam heroTeam : heroTeams) {
            for (Hero hero : heroTeam.getHeroes()) {
                if (hero.equals(round.getFirstHero())) {
                    if (round.getFirstHero().getCurrHealth().doubleValue() > 0) {
                        hero.setCurrHealth(round.getFirstHero().getCurrHealth());
                    } else {
                        hero.setCurrHealth(BigDecimal.ZERO);
                        hero.setAlive(false);
                    }
                } else if (hero.equals(round.getSecondHero())) {
                    if (round.getSecondHero().getCurrHealth().doubleValue() > 0) {
                        hero.setCurrHealth(round.getSecondHero().getCurrHealth());
                    } else {
                        hero.setCurrHealth(BigDecimal.ZERO);
                        hero.setAlive(false);
                    }
                }
            }
        }

        return heroTeams;
    }
}
