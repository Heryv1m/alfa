package ua.teens.alfa.lesson14.arena.model;

import java.math.BigDecimal;
import java.util.Objects;

public class Round {

    private Hero firstHero;
    private Hero secondHero;

    private BigDecimal dpsForFirstHero;
    private BigDecimal dpsForSecondHero;
    private BigDecimal k_armor;
    private BigDecimal k_att;

    public Round(Hero firstHero, Hero secondHero) {
        this.firstHero = firstHero;
        this.secondHero = secondHero;
    }

    public Round(Hero firstHero, Hero secondHero, BigDecimal k_armor, BigDecimal k_att) {
        this.firstHero = firstHero;
        this.secondHero = secondHero;
        this.k_armor = k_armor;
        this.k_att = k_att;
    }

    public void calculateDps() {
         dpsForSecondHero = firstHero.getAttackPower()
                .multiply(k_att)
                .subtract(secondHero.getArmor().multiply(k_armor));
        if (dpsForSecondHero.doubleValue() < 0) {
            dpsForSecondHero = BigDecimal.ZERO;
        }

        dpsForFirstHero = secondHero.getAttackPower()
                .multiply(k_att)
                .subtract(firstHero.getArmor().multiply(k_armor));
        if (dpsForFirstHero.doubleValue() < 0) {
            dpsForFirstHero = BigDecimal.ZERO;
        }
    }

    public Hero getFirstHero() {
        return firstHero;
    }

    public void setFirstHero(Hero firstHero) {
        this.firstHero = firstHero;
    }

    public Hero getSecondHero() {
        return secondHero;
    }

    public void setSecondHero(Hero secondHero) {
        this.secondHero = secondHero;
    }

    public BigDecimal getDpsForFirstHero() {
        return dpsForFirstHero;
    }

    public void setDpsForFirstHero(BigDecimal dpsForFirstHero) {
        this.dpsForFirstHero = dpsForFirstHero;
    }

    public BigDecimal getDpsForSecondHero() {
        return dpsForSecondHero;
    }

    public void setDpsForSecondHero(BigDecimal dpsForSecondHero) {
        this.dpsForSecondHero = dpsForSecondHero;
    }

    public BigDecimal getK_armor() {
        return k_armor;
    }

    public void setK_armor(BigDecimal k_armor) {
        this.k_armor = k_armor;
    }

    public BigDecimal getK_att() {
        return k_att;
    }

    public void setK_att(BigDecimal k_att) {
        this.k_att = k_att;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Round)) return false;
        Round fight = (Round) o;
        return Objects.equals(firstHero, fight.firstHero) &&
                Objects.equals(secondHero, fight.secondHero) &&
                Objects.equals(dpsForFirstHero, fight.dpsForFirstHero) &&
                Objects.equals(dpsForSecondHero, fight.dpsForSecondHero);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstHero, secondHero, dpsForFirstHero, dpsForSecondHero);
    }

    @Override
    public String toString() {
        return "Fight{" +
                "firstHero=" + firstHero +
                ", secondHero=" + secondHero +
                ", dpsForFirstHero=" + dpsForFirstHero +
                ", dpsForSecondHero=" + dpsForSecondHero +
                '}';
    }
}
