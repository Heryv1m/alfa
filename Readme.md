### Primitive types(Class types):
##### Numbers:
- byte (Byte)
- short (Short)
- int (Integer)
- long (Long)


##### With float point:
- float (Float)
- double (Double)


##### Other
- boolean (Boolean)
- char (Character)


### Initialization for primitive types
`
<type> <var_name> = <value>;
`
```
int a = 7;
```

### Initialization for Classes types
```
If you have multiple args, you must split it by commas.
<type> <var_name> = new <type>(<args>);
```
```
// if initialize via default constructor or if your constructor without arguments.
Transaction trx = new Transaction();

// With your custom constructor
Transaction trx = new Transaction(args);
```

### Structural
#### Conditions
##### If
```
if (<condition>) {
    // code that will be execute if <condition> is true
}

if (<condition>) {
    // code that will be execute if <condition> is true
} else {
    // code that will be execute if <condition> is false
}

if (<condition>) {
    // code that will be execute if <condition> is true
} else if (<second condition>) {
    // code that will be execute if <second condition> is true
} else {
   // code that will be execute if all <condition> block are false
}
```
##### Switch
```
switch(<variable for matching>) {
    case "<case for matching>": // code
    case "<second case for matching>": {
        // code
        // if you need stop execution of the case, after it code finish use keyword as return/breack;
    }
    default: {
        // code, that will be executed if no matches in cases above;
    }
}
```

#### Loops
##### For
```
for (<counter initializing>; <condition>; <count iteration>) {
    // code
}
```
If you need iterate though whole loop without counter you can simplify to:
```
for (<single unit array type> <single unit array> : <array name>) {
    // code    
}
```

##### While
Code part will be executed until the condition is true
```
while (<condition>) {
    // code
}
```
`Do` part of the code will be executed anyway once and more than once until the condition is true
```
do {
    // code
} while (<condition>);
```

### Git commands

##### For first initializing repository

* for first initialization your git repository

    `$: git init`
    
* linking your local repo with external on any git platform

    `$: git add remote origin <link to your repository>`


##### For creating commits and push to remote repo
- command for adding files/folder to current commit ('.' equals to all)

    `$: git add <path to file>`

- add description message for all changes in commit

    `$: git commit -m "<commit message>"`

- push all unpushed commit to remote repository

    `$: git push origin <branch name>`


##### For control branches
* check branch name and all changes that was did from last commit

    `$: git status`

* go to existing branch with name <branch name>. If you use -b key, git create new branch and go to this branch with 
code from current branch.

    `$: git checkout (-b) <branch name>`

* show all local branches and mark current branch by asterisk symbol

    `$: git branch`

* delete local branch with name <branch name>. If you use -D key it will be force delete branch 

    `$: git branch -d (-D) <branch name>`

* delete remote branch with name <branch name>.

    `$: git push origin --delete <branch_name>`
